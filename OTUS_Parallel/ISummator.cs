﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTUS_Parallel
{
    interface ISummator
    {
        int Sum(int[] data);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTUS_Parallel
{
    class SummatorService
    {
        private int[] _data;

        public SummatorService(int[] data)
        {
            _data = data;
        }

        public void Sum(ISummator summator)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            int sum = summator.Sum(_data);

            stopwatch.Stop();

            Console.WriteLine($"{summator.GetType().Name}, Count elements={_data.Count()}, Sum={sum}, Total time={stopwatch.ElapsedMilliseconds} ms");
        }
    }
}

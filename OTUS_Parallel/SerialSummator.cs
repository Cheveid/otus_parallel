﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTUS_Parallel
{
    class SerialSummator : ISummator
    {
        public int Sum(int[] data)
        {
            int sum = 0;
            foreach (int i in data)
            {
                sum += i;
            }

            return sum;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTUS_Parallel
{
    class ParallelSummator : ISummator
    {
        public int Sum(int[] data)
        {
            return data.AsParallel().Sum();
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace OTUS_Parallel
{
    class Program
    {
        static int[] dataCount = new int[] { 100_000, 1_000_000, 10_000_000 };

        static void Main(string[] args)
        {
            foreach (int count in dataCount)
            {
                int[] data = DataGenerator.GenerateListInt(count);

                SummatorService summatorService = new SummatorService(data);
                summatorService.Sum(new SerialSummator());
                summatorService.Sum(new ThreadSummator());
                summatorService.Sum(new ParallelSummator());
            }

            Console.ReadLine();
        }
    }
}

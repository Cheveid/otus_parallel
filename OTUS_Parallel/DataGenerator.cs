﻿using System;
using System.Collections.Generic;

namespace OTUS_Parallel
{
    class DataGenerator
    {
        public static int[] GenerateListInt(int countItems)
        {
            int[] list = new int[countItems];

            Random random = new Random();
            for (int i = 0; i < countItems; i++)
            {
                list[i] = random.Next(-1000, 1000);
            }

            return list;
        }
    }
}
